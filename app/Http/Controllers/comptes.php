<?php

namespace App\Http\Controllers;
use App\Models\compte;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class comptes extends Controller
{   
    // UTILISATION DE LA METHODE GET
    public function index(){
        return view('validerr');
    }
    // UTILISATION DE LA METHODE POST
    //VERIFIER  LES INFORMATIONS
    public function creer(Request $request){
    
        $rule=[
            'nom'=>'required',
            'email'=>'required|unique:comptes',
            'age'=>'required',
            'tele'=>'required',
            'pas'=>'required',
            'pass'=>'required|same:pas'
        ];
        $valid= Validator::make($request->all(),$rule);
        $msg=$valid->messages();

        // VERIFIER LES ERREURS DE VALIDATIONS
        if($msg->has('nom')){
            return response()->json([
            'status'=>0,
            'message'=>'entrez votre nom',
            ]);
        }elseif($msg->has('email')){
            return response()->json([
                'status'=>1,
                'message'=>'Email deja pris',
                ]);        
        }elseif($msg->has('age')){
            return response()->json([
                'status'=>2,
                'message'=>'entrez votre Age',
                ]);
        }elseif($msg->has('tele')){
            return response()->json([
                'status'=>3,
                'message'=>'entrez votre Numero',
                ]);
        }elseif($msg->has('pas')){
            return response()->json([
                'status'=>4,
                'message'=>'entrez le Mot de passe',
                ]);
        }elseif($msg->has('pass')){
            return response()->json([
                'status'=>5,
                'message'=>'Comfirmer le mot de passe',
                ]);

        }else{
            $email=$request->email;
            $users= new compte();
            $users->nom=$request->nom;
            $users->email=$request->email;
            $users->age=$request->age;
            $users->telephone=$request->tele;
            $users->password=Hash::make($request->pas);
            $users->pass=$request->pass;
            $a=$users->save();
            // RETOURNER UN MESSAGE SI LES INFORMATIONS SONT ENREGISTRES
            if($a){
            $id=compte::where('email',$email)->value('id');
            return response()->json([
                'id'=>$id,
                'message'=>'cool',
                ]);
            }
        }
    }
// SE CONNECTER 
//VERIFIER L EMAIL ET LE MOT DE PASSE
public function connecte(Request $req){
        if (auth::attempt(['email'=>$req->email,'password'=>$req->password])){
            $emails=$req->email;
    $id=compte::where('email',$emails)->value('id');
            return response()->json([
                'status'=>1,
                'message'=>$id,
                ]);
        }else{
        return response()->json([
        'status'=>0,
        'message'=>'erreur',
        ]);
        }
    }
    // AUTRE CODE CONCERNANT L IMAGE
    public function image(Request $request){
    $nom=time().'-'.$request->file('image')->getClientOriginalName();
    $request->file('image')->move(public_path('fichier'),$nom);
    }
}
