<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// CODE DU COMPTE UTILISATEUR
Route::get('/', function () {
    return view('welcome');
});
Route::get('/form', function () {
    return view('form');
});
// ROOT D ENREGISTREMENT
Route::post('/userss','App\Http\Controllers\comptes@creer');
Route::get('/userss','App\Http\Controllers\comptes@index');
//ROOT DE CONNECTION
Route::post('/connection','App\Http\Controllers\comptes@connecte');



// CODES D EXERCICES
Route::post('/image','App\Http\Controllers\comptes@image');
Route::get('/tache','App\Http\Controllers\moi@afficher');
Route::post('/tache','App\Http\Controllers\moi@ajouter');
Route::post('/tachee','App\Http\Controllers\moi@ajouter');
Route::post('/sup','App\Http\Controllers\moi@sup');
Route::get('/mod/{id}','App\Http\Controllers\moi@mod');
Route::post('/mod','App\Http\Controllers\moi@modifi');






Route::get('/toii','App\Http\Controllers\moi@toi');
Route::get('/zzz','App\Http\Controllers\moi@ou');
Route::post('/prenom','App\Http\Controllers\moi@prenom');
Route::get('/prenom','App\Http\Controllers\moi@prenom');
Route::get('/wow','App\Http\Controllers\moi@bb');
Route::get('/avec','App\Http\Controllers\moi@moi');
Route::get('/b/{id}','App\Http\Controllers\moi@a');
Route::get('/todo','App\Http\Controllers\afaire@faire');
Route::get('/todo','App\Http\Controllers\afaire@index');
Route::get('/detail/{a}','App\Http\Controllers\afaire@info');
Route::get('/sup/{b}','App\Http\Controllers\afaire@sup');
Route::post('/todo','App\Http\Controllers\afaire@faire');
Route::post('/todo','App\Http\Controllers\afaire@store');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
